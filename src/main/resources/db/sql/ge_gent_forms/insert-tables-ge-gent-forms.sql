

---------Table FORMALITE_XML ------------
INSERT INTO formalite_xml VALUES ('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'formalites', '<formalite modelVersion="0"><identifiantFormalite>formalites</identifiantFormalite><adresseSignature><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></adresseSignature><correspondanceAdresse><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></correspondanceAdresse><entreprise><adresse><codePostalCommune/></adresse><entrepriseLieeContratAppui><adresse><codePostalCommune/></adresse></entrepriseLieeContratAppui><entrepriseLieeDomiciliation><adresse><codePostalCommune/></adresse></entrepriseLieeDomiciliation><entrepriseLieeLoueurMandant><adresse><codePostalCommune/></adresse></entrepriseLieeLoueurMandant><etablissement><adresse><codePostalCommune/></adresse><activiteNature>99</activiteNature><activiteNatureAutre>Commecrce de detail sur la marché</activiteNatureAutre><activitePermanenteSaisonniere>P</activitePermanenteSaisonniere><activitePlusImportante>Commerce de détail d''articles de bazar  sur les marches</activitePlusImportante><activites>Commerce de détail d''articles de bazar  sur les marches. </activites><categorie>3</categorie><dateDebutActivite>29/03/2015</dateDebutActivite><effectifSalarieEmbauchePremierSalarie>non</effectifSalarieEmbauchePremierSalarie><enseigne>bazar</enseigne><estAmbulantUE>non</estAmbulantUE><nomCommercialProfessionnel>bazar</nomCommercialProfessionnel><nonSedentariteQualite>ambulant</nonSedentariteQualite><origineFonds>creation</origineFonds><personnePouvoirPresence>non</personnePouvoirPresence><situation>1</situation></etablissement><eirl/><regimeFiscalEirl/><regimeFiscalEirlSecondaire/><regimeFiscalEtablissement><regimeImpositionBenefices>mBIC</regimeImpositionBenefices><regimeImpositionBeneficesVersementLiberatoire>oui</regimeImpositionBeneficesVersementLiberatoire><regimeImpositionTVA>fTVA</regimeImpositionTVA><typeRegime>Principal</typeRegime></regimeFiscalEtablissement><regimeFiscalEtablissementSecondaire/><adresseEntreprisePPSituation>1</adresseEntreprisePPSituation><associeUnique>non</associeUnique><autreEtablissementUE>non</autreEtablissementUE><demandeACCRE>non</demandeACCRE><estActiviteArtisanalePrincipale>non</estActiviteArtisanalePrincipale><insaisissabiliteDeclaration>non</insaisissabiliteDeclaration><nombreDirigeants>1</nombreDirigeants><dirigeants><dirigeant><conjoint><ppAdresse><codePostalCommune/></ppAdresse><ppCivilite>2</ppCivilite><ppDateNaissance>03/03/1996</ppDateNaissance><ppLieuNaissanceCommune>14001</ppLieuNaissanceCommune><ppLieuNaissanceDepartement>14</ppLieuNaissanceDepartement><ppLieuNaissancePays>050</ppLieuNaissancePays><ppNationalite>050</ppNationalite><ppNomNaissance>Berthe</ppNomNaissance><ppPrenom1>Berthe</ppPrenom1></conjoint><ppAdresse><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune><codePays>050</codePays><nomVoie>Az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie></ppAdresse><pmAdresse><codePostalCommune/></pmAdresse><ppAdresseAmbulant/><ppAdresseForain/><pmRepresentantAdresse><codePostalCommune/></pmRepresentantAdresse><declarationSociale><activiteAutreQueDeclareePresence>non</activiteAutreQueDeclareePresence><activiteExerceeAnterieurementPresence>non</activiteExerceeAnterieurementPresence><ayantDroitNombre>1</ayantDroitNombre><ayantDroitPresence>oui</ayantDroitPresence><conjointCouvertAssuranceMaladie2>non</conjointCouvertAssuranceMaladie2><demandeDotationJeuneAgriculteur>non</demandeDotationJeuneAgriculteur><departementOrganismeConventionne>01</departementOrganismeConventionne><numeroSecuriteSociale>000000000000000</numeroSecuriteSociale><optionMicroSocial>oui</optionMicroSocial><optionMicroSocialVersement>T</optionMicroSocialVersement><organismeConventionneCode>N0151</organismeConventionneCode><organismeConventionneCodeReseauNonA>N0151</organismeConventionneCodeReseauNonA><regimeAssuranceMaladie>R</regimeAssuranceMaladie><ayantDroits><ayantDroit><adresse><codePostalCommune/></adresse><identiteComplementCivilite>2</identiteComplementCivilite><identiteComplementDateNaissance>03/03/1996</identiteComplementDateNaissance><identiteComplementLieuNaissanceCommune>14001</identiteComplementLieuNaissanceCommune><identiteComplementLieuNaissanceDepartement>14</identiteComplementLieuNaissanceDepartement><identiteComplementLieuNaissancePays>050</identiteComplementLieuNaissancePays><identiteNationalite>050</identiteNationalite><identiteNomNaissance>Berthe</identiteNomNaissance><identitePrenom1>Berthe</identitePrenom1><lienParente>conjoint</lienParente><niveau>1</niveau><numeroSecuriteSocialeConnu>non</numeroSecuriteSocialeConnu><idTechnique>e491514f-5a58-44d8-b94b-36fe35cb3ee9</idTechnique></ayantDroit></ayantDroits></declarationSociale><conjointPresence>oui</conjointPresence><conjointRegime>oui</conjointRegime><conjointStatut>collaborateur</conjointStatut><nature>1</nature><ppCivilite>1</ppCivilite><ppDateNaissance>01/03/1980</ppDateNaissance><ppLieuNaissancePays>109</ppLieuNaissancePays><ppLieuNaissanceVille>augsbourg</ppLieuNaissanceVille><ppNationalite>109</ppNationalite><ppNomNaissance>Bretch</ppNomNaissance><ppPrenom1>Berti</ppPrenom1><idTechnique>96b9d651-3dbb-4e82-bb8a-9018f83f5fb4</idTechnique></dirigeant></dirigeants></entreprise><correspondanceDestinataire>Brecht</correspondanceDestinataire><courriel>cl@yahoo.fr</courriel><signataireNom>Brecht</signataireNom><signataireQualite>2</signataireQualite><signature>oui</signature><signatureDate>09/01/2017</signatureDate><signatureLieu>Caen</signatureLieu><isAqpa>false</isAqpa><codeCommuneActivite>14118</codeCommuneActivite><dossierId>1</dossierId><estAgentCommercial>false</estAgentCommercial><estAutoEntrepreneur>true</estAutoEntrepreneur><estEirl>false</estEirl><numeroDossier>DOSSIER_C14010002012</numeroDossier><numeroFormalite>C14010002012</numeroFormalite><reseauCFE>C</reseauCFE><terminee>true</terminee><typePersonne>P</typePersonne><utilisateurId>2</utilisateurId><idTechnique>70a9ca34-c907-4e3e-b766-a93cfb6bad2f</idTechnique><version>81a7dc9a</version></formalite>', 'DOSSIER_C14010002012', '2017-01-09', '2017-01-09', '2', '81a7dc9a', '0');
INSERT INTO formalite_xml VALUES ('1f0d2d42-3382-480e-b709-0e583f8d72a4', 'profilEntreprises', '<formalite modelVersion="2"><cfe><adresse1>1 RUE RENE CASSIN</adresse1><adresse2>SAINT-CONTEST</adresse2><adresse3>14911 CAEN CEDEX 9</adresse3><code>C1401</code><fax>0231544077</fax><mail>cfe@caen.cci.fr</mail><nom>CCI CAEN NORMANDIE</nom><observation>Ouvert du lundi au jeudi, de 9h &amp;agrave; 12h30 et de 13h30 &amp;agrave; 17h.&lt;br/&gt;Ouvert le vendredi de 9h &amp;agrave; 12h30.</observation><site>www.caen.cci.fr</site><telephone>0231545485</telephone></cfe><postalCommune><codePostal>14000</codePostal><commune>14118</commune></postalCommune><identifiantFormalite>profilEntreprises</identifiantFormalite><microSocialOuiNon>oui</microSocialOuiNon><activiteAgentCommercial>non</activiteAgentCommercial><activiteNonSalarie>non</activiteNonSalarie><activitePrincipaleCodeActivite>3616</activitePrincipaleCodeActivite><activitePrincipaleDomaine>3608</activitePrincipaleDomaine><activitePrincipaleLibelleActivite>Commerce de détail d''articles de bazar  sur les marches # Commerce de detail sur eventaires et marches</activitePrincipaleLibelleActivite><activitePrincipaleSecteur>COMMERCIAL</activitePrincipaleSecteur><activiteSecondaireLibelleActivite/><ambulant>oui</ambulant><aqpa>false</aqpa><codeDepartement>14</codeDepartement><eirlOuiNon>non</eirlOuiNon><existeActiviteSecondaire>non</existeActiviteSecondaire><formeJuridique>AE</formeJuridique><nomDossier>22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50</nomDossier><reseauCFE>C</reseauCFE><secteurCfe>CCI</secteurCfe><typeCfe>CCI</typeCfe><typePersonne>PP</typePersonne><utilisateurId>2</utilisateurId><idTechnique>1f0d2d42-3382-480e-b709-0e583f8d72a4</idTechnique><version>5fa38262</version><activitePrincipaleNonTrouve>non</activitePrincipaleNonTrouve><evenement>01P</evenement><numeroDossier>DOSSIER_C14010002012</numeroDossier></formalite>', 'DOSSIER_C14010002012', '2017-01-09', '2017-01-09', '2', '5fa38262', '2');

INSERT INTO formalite_xml VALUES ('baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'formalites', '<formalite modelVersion="0"><identifiantFormalite>formalites</identifiantFormalite><adresseSignature><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></adresseSignature><correspondanceAdresse><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></correspondanceAdresse><entreprise><adresse><codePostalCommune/></adresse><entrepriseLieeContratAppui><adresse><codePostalCommune/></adresse></entrepriseLieeContratAppui><entrepriseLieeDomiciliation><adresse><codePostalCommune/></adresse></entrepriseLieeDomiciliation><entrepriseLieeLoueurMandant><adresse><codePostalCommune/></adresse></entrepriseLieeLoueurMandant><etablissement><adresse><codePostalCommune/></adresse><activiteNature>99</activiteNature><activiteNatureAutre>Commecrce de detail sur la marché</activiteNatureAutre><activitePermanenteSaisonniere>P</activitePermanenteSaisonniere><activitePlusImportante>Commerce de détail d''articles de bazar  sur les marches</activitePlusImportante><activites>Commerce de détail d''articles de bazar  sur les marches. </activites><categorie>3</categorie><dateDebutActivite>29/03/2015</dateDebutActivite><effectifSalarieEmbauchePremierSalarie>non</effectifSalarieEmbauchePremierSalarie><enseigne>bazar</enseigne><estAmbulantUE>non</estAmbulantUE><nomCommercialProfessionnel>bazar</nomCommercialProfessionnel><nonSedentariteQualite>ambulant</nonSedentariteQualite><origineFonds>creation</origineFonds><personnePouvoirPresence>non</personnePouvoirPresence><situation>1</situation></etablissement><eirl/><regimeFiscalEirl/><regimeFiscalEirlSecondaire/><regimeFiscalEtablissement><regimeImpositionBenefices>mBIC</regimeImpositionBenefices><regimeImpositionBeneficesVersementLiberatoire>oui</regimeImpositionBeneficesVersementLiberatoire><regimeImpositionTVA>fTVA</regimeImpositionTVA><typeRegime>Principal</typeRegime></regimeFiscalEtablissement><regimeFiscalEtablissementSecondaire/><adresseEntreprisePPSituation>1</adresseEntreprisePPSituation><associeUnique>non</associeUnique><autreEtablissementUE>non</autreEtablissementUE><demandeACCRE>non</demandeACCRE><estActiviteArtisanalePrincipale>non</estActiviteArtisanalePrincipale><insaisissabiliteDeclaration>non</insaisissabiliteDeclaration><nombreDirigeants>1</nombreDirigeants><dirigeants><dirigeant><conjoint><ppAdresse><codePostalCommune/></ppAdresse><ppCivilite>2</ppCivilite><ppDateNaissance>03/03/1996</ppDateNaissance><ppLieuNaissanceCommune>14001</ppLieuNaissanceCommune><ppLieuNaissanceDepartement>14</ppLieuNaissanceDepartement><ppLieuNaissancePays>050</ppLieuNaissancePays><ppNationalite>050</ppNationalite><ppNomNaissance>Berthe</ppNomNaissance><ppPrenom1>Berthe</ppPrenom1></conjoint><ppAdresse><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune><codePays>050</codePays><nomVoie>Az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie></ppAdresse><pmAdresse><codePostalCommune/></pmAdresse><ppAdresseAmbulant/><ppAdresseForain/><pmRepresentantAdresse><codePostalCommune/></pmRepresentantAdresse><declarationSociale><activiteAutreQueDeclareePresence>non</activiteAutreQueDeclareePresence><activiteExerceeAnterieurementPresence>non</activiteExerceeAnterieurementPresence><ayantDroitNombre>1</ayantDroitNombre><ayantDroitPresence>oui</ayantDroitPresence><conjointCouvertAssuranceMaladie2>non</conjointCouvertAssuranceMaladie2><demandeDotationJeuneAgriculteur>non</demandeDotationJeuneAgriculteur><departementOrganismeConventionne>01</departementOrganismeConventionne><numeroSecuriteSociale>000000000000000</numeroSecuriteSociale><optionMicroSocial>oui</optionMicroSocial><optionMicroSocialVersement>T</optionMicroSocialVersement><organismeConventionneCode>N0151</organismeConventionneCode><organismeConventionneCodeReseauNonA>N0151</organismeConventionneCodeReseauNonA><regimeAssuranceMaladie>R</regimeAssuranceMaladie><ayantDroits><ayantDroit><adresse><codePostalCommune/></adresse><identiteComplementCivilite>2</identiteComplementCivilite><identiteComplementDateNaissance>03/03/1996</identiteComplementDateNaissance><identiteComplementLieuNaissanceCommune>14001</identiteComplementLieuNaissanceCommune><identiteComplementLieuNaissanceDepartement>14</identiteComplementLieuNaissanceDepartement><identiteComplementLieuNaissancePays>050</identiteComplementLieuNaissancePays><identiteNationalite>050</identiteNationalite><identiteNomNaissance>Berthe</identiteNomNaissance><identitePrenom1>Berthe</identitePrenom1><lienParente>conjoint</lienParente><niveau>1</niveau><numeroSecuriteSocialeConnu>non</numeroSecuriteSocialeConnu><idTechnique>e491514f-5a58-44d8-b94b-36fe35cb3ee9</idTechnique></ayantDroit></ayantDroits></declarationSociale><conjointPresence>oui</conjointPresence><conjointRegime>oui</conjointRegime><conjointStatut>collaborateur</conjointStatut><nature>1</nature><ppCivilite>1</ppCivilite><ppDateNaissance>01/03/1980</ppDateNaissance><ppLieuNaissancePays>109</ppLieuNaissancePays><ppLieuNaissanceVille>augsbourg</ppLieuNaissanceVille><ppNationalite>109</ppNationalite><ppNomNaissance>Bretch</ppNomNaissance><ppPrenom1>Berti</ppPrenom1><idTechnique>96b9d651-3dbb-4e82-bb8a-9018f83f5fb4</idTechnique></dirigeant></dirigeants></entreprise><correspondanceDestinataire>Brecht</correspondanceDestinataire><courriel>cl@yahoo.fr</courriel><signataireNom>Brecht</signataireNom><signataireQualite>2</signataireQualite><signature>oui</signature><signatureDate>09/01/2017</signatureDate><signatureLieu>Caen</signatureLieu><isAqpa>false</isAqpa><codeCommuneActivite>14118</codeCommuneActivite><dossierId>1</dossierId><estAgentCommercial>false</estAgentCommercial><estAutoEntrepreneur>true</estAutoEntrepreneur><estEirl>false</estEirl><numeroDossier>DOSSIER_C30170307154</numeroDossier><numeroFormalite>C30170307154</numeroFormalite><reseauCFE>C</reseauCFE><terminee>true</terminee><typePersonne>P</typePersonne><utilisateurId>2</utilisateurId><idTechnique>70a9ca34-c907-4e3e-b766-a93cfb6bad2f</idTechnique><version>81a7dc9a</version></formalite>', 'DOSSIER_C30170307154', '2017-01-09', '2017-01-09', '2', '81a7dc9a', '0');
INSERT INTO formalite_xml VALUES ('2f0d2d42-3382-480e-b709-0e583f8d72a4', 'profilEntreprises', '<formalite modelVersion="2"><cfe><adresse1>1 RUE RENE CASSIN</adresse1><adresse2>SAINT-CONTEST</adresse2><adresse3>14911 CAEN CEDEX 9</adresse3><code>C1401</code><fax>0231544077</fax><mail>cfe@caen.cci.fr</mail><nom>CCI CAEN NORMANDIE</nom><observation>Ouvert du lundi au jeudi, de 9h &amp;agrave; 12h30 et de 13h30 &amp;agrave; 17h.&lt;br/&gt;Ouvert le vendredi de 9h &amp;agrave; 12h30.</observation><site>www.caen.cci.fr</site><telephone>0231545485</telephone></cfe><postalCommune><codePostal>14000</codePostal><commune>14118</commune></postalCommune><identifiantFormalite>profilEntreprises</identifiantFormalite><microSocialOuiNon>oui</microSocialOuiNon><activiteAgentCommercial>non</activiteAgentCommercial><activiteNonSalarie>non</activiteNonSalarie><activitePrincipaleCodeActivite>3616</activitePrincipaleCodeActivite><activitePrincipaleDomaine>3608</activitePrincipaleDomaine><activitePrincipaleLibelleActivite>Commerce de détail d''articles de bazar  sur les marches # Commerce de detail sur eventaires et marches</activitePrincipaleLibelleActivite><activitePrincipaleSecteur>COMMERCIAL</activitePrincipaleSecteur><activiteSecondaireLibelleActivite/><ambulant>oui</ambulant><aqpa>false</aqpa><codeDepartement>14</codeDepartement><eirlOuiNon>non</eirlOuiNon><existeActiviteSecondaire>non</existeActiviteSecondaire><formeJuridique>AE</formeJuridique><nomDossier>22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50</nomDossier><reseauCFE>C</reseauCFE><secteurCfe>CCI</secteurCfe><typeCfe>CCI</typeCfe><typePersonne>PP</typePersonne><utilisateurId>2</utilisateurId><idTechnique>1f0d2d42-3382-480e-b709-0e583f8d72a4</idTechnique><version>5fa38262</version><activitePrincipaleNonTrouve>non</activitePrincipaleNonTrouve><evenement>01P</evenement><numeroDossier>DOSSIER_C30170307154</numeroDossier></formalite>', 'DOSSIER_C30170307154', '2017-01-09', '2017-01-09', '2', '5fa38262', '2');

INSERT INTO formalite_xml VALUES ('caaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'formalites', '<formalite modelVersion="0"><identifiantFormalite>formalites</identifiantFormalite><adresseSignature><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></adresseSignature><correspondanceAdresse><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></correspondanceAdresse><entreprise><adresse><codePostalCommune/></adresse><entrepriseLieeContratAppui><adresse><codePostalCommune/></adresse></entrepriseLieeContratAppui><entrepriseLieeDomiciliation><adresse><codePostalCommune/></adresse></entrepriseLieeDomiciliation><entrepriseLieeLoueurMandant><adresse><codePostalCommune/></adresse></entrepriseLieeLoueurMandant><etablissement><adresse><codePostalCommune/></adresse><activiteNature>99</activiteNature><activiteNatureAutre>Commecrce de detail sur la marché</activiteNatureAutre><activitePermanenteSaisonniere>P</activitePermanenteSaisonniere><activitePlusImportante>Commerce de détail d''articles de bazar  sur les marches</activitePlusImportante><activites>Commerce de détail d''articles de bazar  sur les marches. </activites><categorie>3</categorie><dateDebutActivite>29/03/2015</dateDebutActivite><effectifSalarieEmbauchePremierSalarie>non</effectifSalarieEmbauchePremierSalarie><enseigne>bazar</enseigne><estAmbulantUE>non</estAmbulantUE><nomCommercialProfessionnel>bazar</nomCommercialProfessionnel><nonSedentariteQualite>ambulant</nonSedentariteQualite><origineFonds>creation</origineFonds><personnePouvoirPresence>non</personnePouvoirPresence><situation>1</situation></etablissement><eirl/><regimeFiscalEirl/><regimeFiscalEirlSecondaire/><regimeFiscalEtablissement><regimeImpositionBenefices>mBIC</regimeImpositionBenefices><regimeImpositionBeneficesVersementLiberatoire>oui</regimeImpositionBeneficesVersementLiberatoire><regimeImpositionTVA>fTVA</regimeImpositionTVA><typeRegime>Principal</typeRegime></regimeFiscalEtablissement><regimeFiscalEtablissementSecondaire/><adresseEntreprisePPSituation>1</adresseEntreprisePPSituation><associeUnique>non</associeUnique><autreEtablissementUE>non</autreEtablissementUE><demandeACCRE>non</demandeACCRE><estActiviteArtisanalePrincipale>non</estActiviteArtisanalePrincipale><insaisissabiliteDeclaration>non</insaisissabiliteDeclaration><nombreDirigeants>1</nombreDirigeants><dirigeants><dirigeant><conjoint><ppAdresse><codePostalCommune/></ppAdresse><ppCivilite>2</ppCivilite><ppDateNaissance>03/03/1996</ppDateNaissance><ppLieuNaissanceCommune>14001</ppLieuNaissanceCommune><ppLieuNaissanceDepartement>14</ppLieuNaissanceDepartement><ppLieuNaissancePays>050</ppLieuNaissancePays><ppNationalite>050</ppNationalite><ppNomNaissance>Berthe</ppNomNaissance><ppPrenom1>Berthe</ppPrenom1></conjoint><ppAdresse><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune><codePays>050</codePays><nomVoie>Az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie></ppAdresse><pmAdresse><codePostalCommune/></pmAdresse><ppAdresseAmbulant/><ppAdresseForain/><pmRepresentantAdresse><codePostalCommune/></pmRepresentantAdresse><declarationSociale><activiteAutreQueDeclareePresence>non</activiteAutreQueDeclareePresence><activiteExerceeAnterieurementPresence>non</activiteExerceeAnterieurementPresence><ayantDroitNombre>1</ayantDroitNombre><ayantDroitPresence>oui</ayantDroitPresence><conjointCouvertAssuranceMaladie2>non</conjointCouvertAssuranceMaladie2><demandeDotationJeuneAgriculteur>non</demandeDotationJeuneAgriculteur><departementOrganismeConventionne>01</departementOrganismeConventionne><numeroSecuriteSociale>000000000000000</numeroSecuriteSociale><optionMicroSocial>oui</optionMicroSocial><optionMicroSocialVersement>T</optionMicroSocialVersement><organismeConventionneCode>N0151</organismeConventionneCode><organismeConventionneCodeReseauNonA>N0151</organismeConventionneCodeReseauNonA><regimeAssuranceMaladie>R</regimeAssuranceMaladie><ayantDroits><ayantDroit><adresse><codePostalCommune/></adresse><identiteComplementCivilite>2</identiteComplementCivilite><identiteComplementDateNaissance>03/03/1996</identiteComplementDateNaissance><identiteComplementLieuNaissanceCommune>14001</identiteComplementLieuNaissanceCommune><identiteComplementLieuNaissanceDepartement>14</identiteComplementLieuNaissanceDepartement><identiteComplementLieuNaissancePays>050</identiteComplementLieuNaissancePays><identiteNationalite>050</identiteNationalite><identiteNomNaissance>Berthe</identiteNomNaissance><identitePrenom1>Berthe</identitePrenom1><lienParente>conjoint</lienParente><niveau>1</niveau><numeroSecuriteSocialeConnu>non</numeroSecuriteSocialeConnu><idTechnique>e491514f-5a58-44d8-b94b-36fe35cb3ee9</idTechnique></ayantDroit></ayantDroits></declarationSociale><conjointPresence>oui</conjointPresence><conjointRegime>oui</conjointRegime><conjointStatut>collaborateur</conjointStatut><nature>1</nature><ppCivilite>1</ppCivilite><ppDateNaissance>01/03/1980</ppDateNaissance><ppLieuNaissancePays>109</ppLieuNaissancePays><ppLieuNaissanceVille>augsbourg</ppLieuNaissanceVille><ppNationalite>109</ppNationalite><ppNomNaissance>Bretch</ppNomNaissance><ppPrenom1>Berti</ppPrenom1><idTechnique>96b9d651-3dbb-4e82-bb8a-9018f83f5fb4</idTechnique></dirigeant></dirigeants></entreprise><correspondanceDestinataire>Brecht</correspondanceDestinataire><courriel>cl@yahoo.fr</courriel><signataireNom>Brecht</signataireNom><signataireQualite>2</signataireQualite><signature>oui</signature><signatureDate>09/01/2017</signatureDate><signatureLieu>Caen</signatureLieu><isAqpa>false</isAqpa><codeCommuneActivite>14118</codeCommuneActivite><dossierId>1</dossierId><estAgentCommercial>false</estAgentCommercial><estAutoEntrepreneur>true</estAutoEntrepreneur><estEirl>false</estEirl><numeroDossier>DOSSIER_C40170307154</numeroDossier><numeroFormalite>C40170307154</numeroFormalite><reseauCFE>C</reseauCFE><terminee>true</terminee><typePersonne>P</typePersonne><utilisateurId>2</utilisateurId><idTechnique>70a9ca34-c907-4e3e-b766-a93cfb6bad2f</idTechnique><version>81a7dc9a</version></formalite>', 'DOSSIER_C40170307154', '2017-01-09', '2017-01-09', '2', '81a7dc9a', '0');
INSERT INTO formalite_xml VALUES ('3f0d2d42-3382-480e-b709-0e583f8d72a4', 'profilEntreprises', '<formalite modelVersion="2"><cfe><adresse1>1 RUE RENE CASSIN</adresse1><adresse2>SAINT-CONTEST</adresse2><adresse3>14911 CAEN CEDEX 9</adresse3><code>C1401</code><fax>0231544077</fax><mail>cfe@caen.cci.fr</mail><nom>CCI CAEN NORMANDIE</nom><observation>Ouvert du lundi au jeudi, de 9h &amp;agrave; 12h30 et de 13h30 &amp;agrave; 17h.&lt;br/&gt;Ouvert le vendredi de 9h &amp;agrave; 12h30.</observation><site>www.caen.cci.fr</site><telephone>0231545485</telephone></cfe><postalCommune><codePostal>14000</codePostal><commune>14118</commune></postalCommune><identifiantFormalite>profilEntreprises</identifiantFormalite><microSocialOuiNon>oui</microSocialOuiNon><activiteAgentCommercial>non</activiteAgentCommercial><activiteNonSalarie>non</activiteNonSalarie><activitePrincipaleCodeActivite>3616</activitePrincipaleCodeActivite><activitePrincipaleDomaine>3608</activitePrincipaleDomaine><activitePrincipaleLibelleActivite>Commerce de détail d''articles de bazar  sur les marches # Commerce de detail sur eventaires et marches</activitePrincipaleLibelleActivite><activitePrincipaleSecteur>COMMERCIAL</activitePrincipaleSecteur><activiteSecondaireLibelleActivite/><ambulant>oui</ambulant><aqpa>false</aqpa><codeDepartement>14</codeDepartement><eirlOuiNon>non</eirlOuiNon><existeActiviteSecondaire>non</existeActiviteSecondaire><formeJuridique>AE</formeJuridique><nomDossier>22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50</nomDossier><reseauCFE>C</reseauCFE><secteurCfe>CCI</secteurCfe><typeCfe>CCI</typeCfe><typePersonne>PP</typePersonne><utilisateurId>2</utilisateurId><idTechnique>1f0d2d42-3382-480e-b709-0e583f8d72a4</idTechnique><version>5fa38262</version><activitePrincipaleNonTrouve>non</activitePrincipaleNonTrouve><evenement>01P</evenement><numeroDossier>DOSSIER_C40170307154</numeroDossier></formalite>', 'DOSSIER_C40170307154', '2017-01-09', '2017-01-09', '2', '5fa38262', '2');

INSERT INTO formalite_xml VALUES ('daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'formalites', '<formalite modelVersion="0"><identifiantFormalite>formalites</identifiantFormalite><adresseSignature><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></adresseSignature><correspondanceAdresse><codePays>050</codePays><nomVoie>az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune></correspondanceAdresse><entreprise><adresse><codePostalCommune/></adresse><entrepriseLieeContratAppui><adresse><codePostalCommune/></adresse></entrepriseLieeContratAppui><entrepriseLieeDomiciliation><adresse><codePostalCommune/></adresse></entrepriseLieeDomiciliation><entrepriseLieeLoueurMandant><adresse><codePostalCommune/></adresse></entrepriseLieeLoueurMandant><etablissement><adresse><codePostalCommune/></adresse><activiteNature>99</activiteNature><activiteNatureAutre>Commecrce de detail sur la marché</activiteNatureAutre><activitePermanenteSaisonniere>P</activitePermanenteSaisonniere><activitePlusImportante>Commerce de détail d''articles de bazar  sur les marches</activitePlusImportante><activites>Commerce de détail d''articles de bazar  sur les marches. </activites><categorie>3</categorie><dateDebutActivite>29/03/2015</dateDebutActivite><effectifSalarieEmbauchePremierSalarie>non</effectifSalarieEmbauchePremierSalarie><enseigne>bazar</enseigne><estAmbulantUE>non</estAmbulantUE><nomCommercialProfessionnel>bazar</nomCommercialProfessionnel><nonSedentariteQualite>ambulant</nonSedentariteQualite><origineFonds>creation</origineFonds><personnePouvoirPresence>non</personnePouvoirPresence><situation>1</situation></etablissement><eirl/><regimeFiscalEirl/><regimeFiscalEirlSecondaire/><regimeFiscalEtablissement><regimeImpositionBenefices>mBIC</regimeImpositionBenefices><regimeImpositionBeneficesVersementLiberatoire>oui</regimeImpositionBeneficesVersementLiberatoire><regimeImpositionTVA>fTVA</regimeImpositionTVA><typeRegime>Principal</typeRegime></regimeFiscalEtablissement><regimeFiscalEtablissementSecondaire/><adresseEntreprisePPSituation>1</adresseEntreprisePPSituation><associeUnique>non</associeUnique><autreEtablissementUE>non</autreEtablissementUE><demandeACCRE>non</demandeACCRE><estActiviteArtisanalePrincipale>non</estActiviteArtisanalePrincipale><insaisissabiliteDeclaration>non</insaisissabiliteDeclaration><nombreDirigeants>1</nombreDirigeants><dirigeants><dirigeant><conjoint><ppAdresse><codePostalCommune/></ppAdresse><ppCivilite>2</ppCivilite><ppDateNaissance>03/03/1996</ppDateNaissance><ppLieuNaissanceCommune>14001</ppLieuNaissanceCommune><ppLieuNaissanceDepartement>14</ppLieuNaissanceDepartement><ppLieuNaissancePays>050</ppLieuNaissancePays><ppNationalite>050</ppNationalite><ppNomNaissance>Berthe</ppNomNaissance><ppPrenom1>Berthe</ppPrenom1></conjoint><ppAdresse><codePostalCommune><codePostal>14000</codePostal><commune>14118</commune></codePostalCommune><codePays>050</codePays><nomVoie>Az</nomVoie><numeroVoie>1</numeroVoie><typeVoie>ALL</typeVoie></ppAdresse><pmAdresse><codePostalCommune/></pmAdresse><ppAdresseAmbulant/><ppAdresseForain/><pmRepresentantAdresse><codePostalCommune/></pmRepresentantAdresse><declarationSociale><activiteAutreQueDeclareePresence>non</activiteAutreQueDeclareePresence><activiteExerceeAnterieurementPresence>non</activiteExerceeAnterieurementPresence><ayantDroitNombre>1</ayantDroitNombre><ayantDroitPresence>oui</ayantDroitPresence><conjointCouvertAssuranceMaladie2>non</conjointCouvertAssuranceMaladie2><demandeDotationJeuneAgriculteur>non</demandeDotationJeuneAgriculteur><departementOrganismeConventionne>01</departementOrganismeConventionne><numeroSecuriteSociale>000000000000000</numeroSecuriteSociale><optionMicroSocial>oui</optionMicroSocial><optionMicroSocialVersement>T</optionMicroSocialVersement><organismeConventionneCode>N0151</organismeConventionneCode><organismeConventionneCodeReseauNonA>N0151</organismeConventionneCodeReseauNonA><regimeAssuranceMaladie>R</regimeAssuranceMaladie><ayantDroits><ayantDroit><adresse><codePostalCommune/></adresse><identiteComplementCivilite>2</identiteComplementCivilite><identiteComplementDateNaissance>03/03/1996</identiteComplementDateNaissance><identiteComplementLieuNaissanceCommune>14001</identiteComplementLieuNaissanceCommune><identiteComplementLieuNaissanceDepartement>14</identiteComplementLieuNaissanceDepartement><identiteComplementLieuNaissancePays>050</identiteComplementLieuNaissancePays><identiteNationalite>050</identiteNationalite><identiteNomNaissance>Berthe</identiteNomNaissance><identitePrenom1>Berthe</identitePrenom1><lienParente>conjoint</lienParente><niveau>1</niveau><numeroSecuriteSocialeConnu>non</numeroSecuriteSocialeConnu><idTechnique>e491514f-5a58-44d8-b94b-36fe35cb3ee9</idTechnique></ayantDroit></ayantDroits></declarationSociale><conjointPresence>oui</conjointPresence><conjointRegime>oui</conjointRegime><conjointStatut>collaborateur</conjointStatut><nature>1</nature><ppCivilite>1</ppCivilite><ppDateNaissance>01/03/1980</ppDateNaissance><ppLieuNaissancePays>109</ppLieuNaissancePays><ppLieuNaissanceVille>augsbourg</ppLieuNaissanceVille><ppNationalite>109</ppNationalite><ppNomNaissance>Bretch</ppNomNaissance><ppPrenom1>Berti</ppPrenom1><idTechnique>96b9d651-3dbb-4e82-bb8a-9018f83f5fb4</idTechnique></dirigeant></dirigeants></entreprise><correspondanceDestinataire>Brecht</correspondanceDestinataire><courriel>cl@yahoo.fr</courriel><signataireNom>Brecht</signataireNom><signataireQualite>2</signataireQualite><signature>oui</signature><signatureDate>09/01/2017</signatureDate><signatureLieu>Caen</signatureLieu><isAqpa>false</isAqpa><codeCommuneActivite>14118</codeCommuneActivite><dossierId>1</dossierId><estAgentCommercial>false</estAgentCommercial><estAutoEntrepreneur>true</estAutoEntrepreneur><estEirl>false</estEirl><numeroDossier>DOSSIER_C50170307154</numeroDossier><numeroFormalite>C50170307154</numeroFormalite><reseauCFE>C</reseauCFE><terminee>true</terminee><typePersonne>P</typePersonne><utilisateurId>2</utilisateurId><idTechnique>70a9ca34-c907-4e3e-b766-a93cfb6bad2f</idTechnique><version>81a7dc9a</version></formalite>', 'DOSSIER_C50170307154', '2017-01-09', '2017-01-09', '2', '81a7dc9a', '0');
INSERT INTO formalite_xml VALUES ('4f0d2d42-3382-480e-b709-0e583f8d72a4', 'profilEntreprises', '<formalite modelVersion="2"><cfe><adresse1>1 RUE RENE CASSIN</adresse1><adresse2>SAINT-CONTEST</adresse2><adresse3>14911 CAEN CEDEX 9</adresse3><code>C1401</code><fax>0231544077</fax><mail>cfe@caen.cci.fr</mail><nom>CCI CAEN NORMANDIE</nom><observation>Ouvert du lundi au jeudi, de 9h &amp;agrave; 12h30 et de 13h30 &amp;agrave; 17h.&lt;br/&gt;Ouvert le vendredi de 9h &amp;agrave; 12h30.</observation><site>www.caen.cci.fr</site><telephone>0231545485</telephone></cfe><postalCommune><codePostal>14000</codePostal><commune>14118</commune></postalCommune><identifiantFormalite>profilEntreprises</identifiantFormalite><microSocialOuiNon>oui</microSocialOuiNon><activiteAgentCommercial>non</activiteAgentCommercial><activiteNonSalarie>non</activiteNonSalarie><activitePrincipaleCodeActivite>3616</activitePrincipaleCodeActivite><activitePrincipaleDomaine>3608</activitePrincipaleDomaine><activitePrincipaleLibelleActivite>Commerce de détail d''articles de bazar  sur les marches # Commerce de detail sur eventaires et marches</activitePrincipaleLibelleActivite><activitePrincipaleSecteur>COMMERCIAL</activitePrincipaleSecteur><activiteSecondaireLibelleActivite/><ambulant>oui</ambulant><aqpa>false</aqpa><codeDepartement>14</codeDepartement><eirlOuiNon>non</eirlOuiNon><existeActiviteSecondaire>non</existeActiviteSecondaire><formeJuridique>AE</formeJuridique><nomDossier>22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50</nomDossier><reseauCFE>C</reseauCFE><secteurCfe>CCI</secteurCfe><typeCfe>CCI</typeCfe><typePersonne>PP</typePersonne><utilisateurId>2</utilisateurId><idTechnique>1f0d2d42-3382-480e-b709-0e583f8d72a4</idTechnique><version>5fa38262</version><activitePrincipaleNonTrouve>non</activitePrincipaleNonTrouve><evenement>01P</evenement><numeroDossier>DOSSIER_C50170307154</numeroDossier></formalite>', 'DOSSIER_C50170307154', '2017-01-09', '2017-01-09', '2', '5fa38262', '2');

---------Table DOCUMENT_CERFA ------------
INSERT INTO document_cerfa VALUES ('DOSSIER_C14010002012', '/var/data/ge/gent/pj/2/DOSSIER_C14010002012/201701100000_DOSSIER_C14010002012_creation.pdf', false, '2017-01-10', 1);
INSERT INTO document_cerfa VALUES ('DOSSIER_C14010002013', '/var/data/ge/gent/pj/2/DOSSIER_C14010002012/201701100000_DOSSIER_C14010002012_creation.pdf', false, '2017-01-10', 2);

---------Table ETAT ------------
INSERT INTO etat VALUES ('1', 'En cours de saisie');
INSERT INTO etat VALUES ('0', 'En cours de préparation');
INSERT INTO etat VALUES ('9', 'Supprimé');
INSERT INTO etat VALUES ('3', 'Problème');
INSERT INTO etat VALUES ('4', 'Payé');
INSERT INTO etat VALUES ('5', 'Envoyé');
INSERT INTO etat VALUES ('7', 'Demande acceptée');
INSERT INTO etat VALUES ('31', 'Problème SES');
INSERT INTO etat VALUES ('41', 'Sans Paiement');
INSERT INTO etat VALUES ('10', 'A retraiter');
INSERT INTO etat VALUES ('2', 'Complété');
INSERT INTO etat VALUES ('6', 'Mis à disposition des destinataires');

---------Table DOSSIER ------------
INSERT INTO dossier VALUES (1, '5', '2', '2017-01-09 23:58:41.737', '2017-01-10 00:01:39.334', 'DOSSIER_C14010002012', '22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', NULL, 0, 'H10000000106');
INSERT INTO dossier VALUES (2, '5', '2', '2017-01-09 23:58:41.737', '2017-01-10 00:01:39.334', 'DOSSIER_C14010002013', '23 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', NULL, 0, 'H10000000106');
INSERT INTO dossier VALUES (3, '5', '2', '2017-01-09 23:58:41.737', '2017-01-10 00:01:39.334', 'DOSSIER_C14010002014', '24 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', NULL, 0, '');
INSERT INTO dossier VALUES (4, '9', '2', '2017-01-09 23:58:41.737', '2017-01-10 00:01:39.334', 'DOSSIER_C14010002015', '25 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');
INSERT INTO dossier VALUES (5, '0', '3', '2016-01-09 23:58:41.737', '2017-01-10 00:01:39.334', 'DOSSIER_C20170307153', '26 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');
INSERT INTO dossier VALUES (6, '6', '3', '2016-01-09 23:58:41.737', '2016-01-10 00:01:39.334', 'DOSSIER_C20170307154', '26 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');
INSERT INTO dossier VALUES (7, '2', '4', '2016-01-09 23:58:41.737', '2016-01-11 00:01:39.334', 'DOSSIER_C30170307154', '22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');
INSERT INTO dossier VALUES (8, '2', '4', '2016-01-10 23:58:41.737', '2016-01-12 00:01:39.334', 'DOSSIER_C40170307154', '22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');
INSERT INTO dossier VALUES (9, '2', '4', '2016-01-11 23:58:41.737', '2016-01-13 00:01:39.334', 'DOSSIER_C50170307154', '22 - PP ME - CAEN - KNAy-AROO - 09/01/2017 à 23:45:50', NULL, 'Commerce de détail d''articles de bazar  sur les marches', '2017-01-11', 0, 'H10000000107');


---------Table piece_justificative_xml ------------
INSERT INTO piece_justificative_xml VALUES ('4649c071-027c-4d10-a182-2d3ffa69b490', 'PJ020', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ020</codePj><libelle>Un exemplaire de l''attestation de délivrance de l''information donnée à son conjoint des
conséquences sur les biens communs des dettes contractées dans l''exercice de sa profession</libelle><famille>Pièces relatives aux personnes liées</famille><categorie>Autre type de pièce jointe</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP029</idReglesDeclencheurs><dateCreation>2017-01-09T23:59:39.418+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/4649c071-027c-4d10-a182-2d3ffa69b490/</cheminStockage><idTechnique>4649c071-027c-4d10-a182-2d3ffa69b490</idTechnique><version>b2553e7c</version><nomFichiersRattachesPJ><id>2997ac5d-e299-49df-b12a-2eeccd3e9d8a</id><nom>PJ__Justificatif_de_domicile_de_mo</nom><extension>pdf</extension></nomFichiersRattachesPJ><dateMaj>2017-01-09T23:59:45.868+01:00</dateMaj></pieceJustificative>', '2017-01-09', '2017-01-09', '2', 'b2553e7c', '1');
INSERT INTO piece_justificative_xml VALUES ('5fef5fa3-a722-4e76-99d1-6b3e8dd7e8a5', 'PJ001', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ001</codePj><libelle>Une copie recto/verso de la carte nationale d''identité, du passeport du signataire de la formalité (pages correspondant à l''identité et à la validité de la pièce), du titre ou récépissé du titre de séjour (le statut porté sur le titre de séjour doit permettre à son titulaire de s''inscrire au RCS / RSAC / RM / RBA) en cours de validité. La copie numérisée du justificatif d''identité doit revêtir une mention manuscrite d''attestation sur l''honneur de conformité à l''original, une date et la signature manuscrite de la personne qui effectue la déclaration.</libelle><famille>Pièces relatives au signataire</famille><categorie>Carte d''identité</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP005</idReglesDeclencheurs><dateCreation>2017-01-09T23:59:39.384+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/5fef5fa3-a722-4e76-99d1-6b3e8dd7e8a5/</cheminStockage><idTechnique>5fef5fa3-a722-4e76-99d1-6b3e8dd7e8a5</idTechnique><version>c76f7bda</version><nomFichiersRattachesPJ><id>d5004c5e-6a7e-49ab-9281-49ed4056ee6f</id><nom>PJ__l_accord_du_conjoint_en_cas_d_</nom><extension>pdf</extension></nomFichiersRattachesPJ><dateMaj>2017-01-09T23:59:48.917+01:00</dateMaj></pieceJustificative>', '2017-01-09', '2017-01-09', '2', 'c76f7bda', '1');
INSERT INTO piece_justificative_xml VALUES ('94853324-558e-4f88-852b-4630f68fcd9c', 'PJ011', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ011</codePj><libelle>Justificatif de domicile de moins de trois mois au  nom et prénom du dirigeant ou au nom et prénom de l''hébergeant accompagné d''une attestation d''hébergement</libelle><famille>Pièces relatives à l''établissement (PP) ou au siège social (PM)</famille><categorie>Attestation de domicile</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP020</idReglesDeclencheurs><dateCreation>2017-01-09T23:59:39.407+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/94853324-558e-4f88-852b-4630f68fcd9c/</cheminStockage><idTechnique>94853324-558e-4f88-852b-4630f68fcd9c</idTechnique><version>952dddbc</version><nomFichiersRattachesPJ><id>39e46f02-602b-4d22-b00f-2b81c12e98a5</id><nom>PJ__Une_copie_recto_verso_de_la_ca</nom><extension>pdf</extension></nomFichiersRattachesPJ><dateMaj>2017-01-09T23:59:51.820+01:00</dateMaj></pieceJustificative>', '2017-01-09', '2017-01-09', '2', '952dddbc', '1');
INSERT INTO piece_justificative_xml VALUES ('a4537d2c-ad6e-43c2-ac1f-2ebe17f93d0a', 'PJ003', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ003</codePj><libelle>Le cas échéant fournir un pouvoir (procuration permettant à un tiers de signer la formalité lorsqu''il ne s''agit pas du dirigeant de l''entreprise) sinon joindre un fichier vierge (PDF ou JPEG)</libelle><famille>Pièces relatives au signataire</famille><categorie>Autre type de pièce jointe</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP012</idReglesDeclencheurs><dateCreation>2017-01-09T23:59:39.397+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/a4537d2c-ad6e-43c2-ac1f-2ebe17f93d0a/</cheminStockage><idTechnique>a4537d2c-ad6e-43c2-ac1f-2ebe17f93d0a</idTechnique><version>3eb6ec4</version><nomFichiersRattachesPJ><id>d4ef17ee-09e3-4918-842b-17e0145e1729</id><nom>PJ__Acte_de_nomination_du_repr_sen</nom><extension>pdf</extension></nomFichiersRattachesPJ><dateMaj>2017-01-09T23:59:53.090+01:00</dateMaj></pieceJustificative>', '2017-01-09', '2017-01-09', '2', '3eb6ec4', '1');
INSERT INTO piece_justificative_xml VALUES ('467363f8-4616-4b8b-8ad5-6a0552a06fe3', 'PJ021', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ021</codePj><libelle>Un justificatif d''identité du conjoint faisant état du mariage (copie de la carte nationale d''identité, extrait d''acte de mariage, copie du livret de famille, extrait d''acte de naissance), le certificat de pacs délivré par le tribunal d''instance, un extrait d''acte de naissance, ou tout autre document officiel faisant état de l''union</libelle><famille>Pièces relatives aux personnes liées</famille><categorie>Carte d''identité</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP030</idReglesDeclencheurs><dateCreation>2017-01-09T23:59:39.428+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/467363f8-4616-4b8b-8ad5-6a0552a06fe3/</cheminStockage><idTechnique>467363f8-4616-4b8b-8ad5-6a0552a06fe3</idTechnique><version>783b9ef1</version><nomFichiersRattachesPJ><id>894980cf-27c7-4700-a865-b540eed7f45e</id><nom>PJ__Une_copie_recto_verso_de_la_ca</nom><extension>pdf</extension></nomFichiersRattachesPJ><dateMaj>2017-01-09T23:59:47.433+01:00</dateMaj></pieceJustificative>', '2017-01-09', '2017-01-09', '2', '783b9ef1', '1');
INSERT INTO piece_justificative_xml VALUES ('33e4d44d-0c08-401f-abe6-0d8722878239', 'PJ004', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ004</codePj><libelle>Une copie recto/verso de la carte nationale d''identité, du passeport (pages correspondant à l''identité et à la validité de la pièce), du titre ou récépissé du titre de séjour (le statut porté sur le titre de séjour doit permettre à son titulaire de s''inscrire, le cas échéant, au RCS / RM /RSAC : carte de résident, carte de séjour portant mention de l''activité commerciale, industrielle ou artisanale autorisée, ou « vie privée et familiale », « compétences et talents », « toute profession dans le cadre de la législation en vigueur », cette liste n''étant pas exhaustive) ou d''un autre document officiel (liste fixée par décret) à jour et en cours de validité.</libelle><famille>Pièces relatives aux dirigeants</famille><categorie>Carte d''identité</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP013</idReglesDeclencheurs><dateCreation>2017-01-09T23:58:43.717+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/33e4d44d-0c08-401f-abe6-0d8722878239/</cheminStockage><idTechnique>33e4d44d-0c08-401f-abe6-0d8722878239</idTechnique><version>e6438fe7</version><codeDestinataires><code>C1401</code></codeDestinataires><idReglesDeclencheurs>PP001</idReglesDeclencheurs><dateMaj>2017-01-09T23:59:44.466+01:00</dateMaj><idReglesDeclencheurs>PP013</idReglesDeclencheurs><idReglesDeclencheurs>PP001</idReglesDeclencheurs><nomFichiersRattachesPJ><id>0eee7e5b-a4e2-493b-a16f-995b58d02510</id><nom>PJ__Extrait_RCS_de_moins_de_3_mois</nom><extension>pdf</extension></nomFichiersRattachesPJ></pieceJustificative>', '2017-01-09', '2017-01-09', '2', 'e6438fe7', '1');
INSERT INTO piece_justificative_xml VALUES ('72f94c7a-54e5-45a1-aca3-88157a642e9c', 'PJ006', 'C14010002012', 'DOSSIER_C14010002012', '<pieceJustificative modelVersion="1"><codePj>PJ006</codePj><libelle>Une déclaration sur l''honneur de non condamnation faisant apparaître la filiation</libelle><famille>Pièces relatives aux dirigeants</famille><categorie>Autre type de pièce jointe</categorie><utilisateurId>2</utilisateurId><identifiantFormaliteLiee>C14010002012</identifiantFormaliteLiee><numeroDossier>DOSSIER_C14010002012</numeroDossier><codeDestinataires><code>G1402</code></codeDestinataires><idReglesDeclencheurs>PP015</idReglesDeclencheurs><dateCreation>2017-01-09T23:58:43.731+01:00</dateCreation><cheminStockage>/var/data/ge/gent/pj/2/DOSSIER_C14010002012/72f94c7a-54e5-45a1-aca3-88157a642e9c/</cheminStockage><idTechnique>72f94c7a-54e5-45a1-aca3-88157a642e9c</idTechnique><version>8ef7b1ac</version><idReglesDeclencheurs>PP015</idReglesDeclencheurs><dateMaj>2017-01-09T23:59:50.451+01:00</dateMaj><nomFichiersRattachesPJ><id>dfabd976-0964-495d-bbac-f8ecf350f58f</id><nom>PJ__Justificatif_d_identit_de_l_e</nom><extension>pdf</extension></nomFichiersRattachesPJ></pieceJustificative>', '2017-01-09', '2017-01-09', '2', '8ef7b1ac', '1');

---------Table paiement_formalite ------------
INSERT INTO paiement_formalite VALUES ('C14010002012', 'NOUVEAU', '<paiementFormalite><etat>NOUVEAU</etat><idFormalite>C14010002012</idFormalite><mailClient>cl@yahoo.fr</mailClient><montantTotal>0</montantTotal><montantTotalTousMoyens>0</montantTotalTousMoyens></paiementFormalite>', 1, false);

---------Table paiementencours ------------
INSERT INTO paiementencours VALUES (1, 1, 'C14010002012', NULL, '2017-01-09 23:58:41.737', '2017-01-09 23:58:41.737', '1', 10, 'C7501');

---------Table paiement_effectue ------------
INSERT INTO paiement_effectue VALUES (1, 'C14010002012', '1', 'C7501', 10, '2017-01-09 23:58:41.737');

---------Table documentssignes ------------
INSERT INTO documentssignes VALUES (1, 'DOSSIER_C14010002012', 'fr', '2017-01-09');

---------Table destinataire_dossier ------------
INSERT INTO destinataire_dossier VALUES (5, 'C1401', 1, 'CFE');
INSERT INTO destinataire_dossier VALUES (6, 'G1402', 1, 'TDR');

---------Table rapport_exec ------------
INSERT INTO rapport_exec VALUES ('1', 'DOSSIER_C14010002012', '12', '1', '1', '1', 'libelle', 'statut');

---------Table traitementenattente ------------
INSERT INTO traitementenattente VALUES (1, 1, '2017-01-09 23:58:41.737', '2017-01-09 23:58:41.737', '2017-01-09 23:58:41.737', 1);

---------Table historiquedossier ------------
INSERT INTO historiquedossier VALUES (1, 1, 'Création du dossier de création', '2017-01-09', 0);
INSERT INTO historiquedossier VALUES (2, 1, 'Validation de la formalité C14010002012', '2017-01-09', 1);
INSERT INTO historiquedossier VALUES (3, 1, 'Ajout de toutes les pièces justificatives', '2017-01-09', 2);
INSERT INTO historiquedossier VALUES (4, 1, 'Dossier complété C14010002012', '2017-01-10', 3);

---------Table historiquetechniquedossier ------------
INSERT INTO historiquetechniquedossier VALUES (1, 'DOSSIER_C14010002012', '0', '2017-01-09 23:58:41.762');
INSERT INTO historiquetechniquedossier VALUES (2, 'DOSSIER_C14010002012', '41', '2017-01-10 00:00:08.416');
INSERT INTO historiquetechniquedossier VALUES (3, 'DOSSIER_C14010002012', '5', '2017-01-10 00:01:39.339');
INSERT INTO historiquetechniquedossier VALUES (4, 'DOSSIER_C14010002012', '6', '2017-01-10 11:57:22.408');



---------Table regimefiscal ------------
INSERT INTO regimefiscal VALUES (1, 'dcBNC', NULL, NULL, 'fTVA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO regimefiscal VALUES (2, 'rnBIC', NULL, 'optionIS', 'rnTVA', true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO regimefiscal VALUES (3, 'rnBIC', NULL, 'optionIS', 'rnTVA', true, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

---------Table eirl ------------
INSERT INTO eirl VALUES (1, NULL, NULL, 'O', 'Dupont Rene', '2017-12-31', 'Reparation et vente de vehicule', true, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO eirl VALUES (2, NULL, NULL, 'O', 'Dupont Rene', '2017-12-31', 'Reparation et vente de vehicule', true, NULL, 2, NULL, NULL, NULL, NULL);

---------Table pays ------------
INSERT INTO pays VALUES (1, NULL, NULL, NULL);
INSERT INTO pays VALUES (2, NULL, NULL, NULL);
INSERT INTO pays VALUES (4, NULL, NULL, NULL);
INSERT INTO pays VALUES (6, NULL, NULL, NULL);
INSERT INTO pays VALUES (5, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (7, NULL, NULL, NULL);
INSERT INTO pays VALUES (8, NULL, NULL, NULL);
INSERT INTO pays VALUES (9, NULL, NULL, NULL);
INSERT INTO pays VALUES (10, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (11, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (3, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (15, NULL, NULL, NULL);
INSERT INTO pays VALUES (14, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (16, NULL, NULL, NULL);
INSERT INTO pays VALUES (17, NULL, NULL, NULL);
INSERT INTO pays VALUES (18, NULL, NULL, NULL);
INSERT INTO pays VALUES (19, NULL, NULL, NULL);
INSERT INTO pays VALUES (20, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (21, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (22, NULL, NULL, NULL);
INSERT INTO pays VALUES (23, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (24, NULL, NULL, NULL);
INSERT INTO pays VALUES (25, NULL, NULL, NULL);
INSERT INTO pays VALUES (26, NULL, NULL, NULL);
INSERT INTO pays VALUES (27, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (12, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (13, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (31, NULL, NULL, NULL);
INSERT INTO pays VALUES (30, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (32, NULL, NULL, NULL);
INSERT INTO pays VALUES (33, NULL, NULL, NULL);
INSERT INTO pays VALUES (34, NULL, NULL, NULL);
INSERT INTO pays VALUES (35, NULL, NULL, NULL);
INSERT INTO pays VALUES (36, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (37, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (38, NULL, NULL, NULL);
INSERT INTO pays VALUES (39, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (40, NULL, NULL, NULL);
INSERT INTO pays VALUES (41, NULL, NULL, NULL);
INSERT INTO pays VALUES (42, NULL, NULL, NULL);
INSERT INTO pays VALUES (43, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (28, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (29, 'FRANCE', NULL, '050');
INSERT INTO pays VALUES (44, NULL, NULL, NULL);
INSERT INTO pays VALUES (45, NULL, NULL, NULL);
INSERT INTO pays VALUES (46, NULL, NULL, NULL);
INSERT INTO pays VALUES (47, NULL, NULL, NULL);

---------Table commune ------------
INSERT INTO commune VALUES (1, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (2, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (4, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (6, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (5, 'BOBIGNY', '93008', '93000', NULL);
INSERT INTO commune VALUES (7, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (8, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (9, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (10, 'BOBIGNY', '93008', '93000', NULL);
INSERT INTO commune VALUES (11, 'ARCHINGEAY', '17017', NULL, NULL);
INSERT INTO commune VALUES (12, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (3, 'BOBIGNY', '93008', '93000', NULL);
INSERT INTO commune VALUES (16, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (15, 'CHARTRES', '28085', '28000', NULL);
INSERT INTO commune VALUES (17, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (18, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (19, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (20, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (21, 'CHARTRES', '28085', '28000', NULL);
INSERT INTO commune VALUES (22, 'OUISTREHAM', '14488', NULL, NULL);
INSERT INTO commune VALUES (23, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (24, 'VERSAILLES', '78646', NULL, NULL);
INSERT INTO commune VALUES (25, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (26, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (27, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (28, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (29, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (30, 'VERSAILLES', '78646', NULL, NULL);
INSERT INTO commune VALUES (31, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (13, 'CHARTRES', '28085', '28000', NULL);
INSERT INTO commune VALUES (14, 'CHARTRES', '28085', '28000', NULL);
INSERT INTO commune VALUES (35, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (34, 'QUIMPER', '29232', '29000', NULL);
INSERT INTO commune VALUES (36, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (37, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (38, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (39, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (40, 'QUIMPER', '29232', '29000', NULL);
INSERT INTO commune VALUES (41, 'AUFFERVILLE', '77011', NULL, NULL);
INSERT INTO commune VALUES (42, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (43, 'VERSAILLES', '78646', NULL, NULL);
INSERT INTO commune VALUES (44, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (45, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (46, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (47, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (48, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (49, 'VERSAILLES', '78646', NULL, NULL);
INSERT INTO commune VALUES (50, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (32, 'QUIMPER', '29232', '29000', NULL);
INSERT INTO commune VALUES (33, 'QUIMPER', '29232', '29000', NULL);
INSERT INTO commune VALUES (51, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (52, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (53, NULL, NULL, NULL, NULL);
INSERT INTO commune VALUES (54, NULL, NULL, NULL, NULL);

---------Table adresse ------------
INSERT INTO adresse VALUES (1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (2, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (4, 4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (6, 6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (5, 5, 5, NULL, NULL, 'RUE', 'dzadza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (7, 7, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (8, 8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (9, 9, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (10, 10, 10, NULL, NULL, 'RUE', 'dzadza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (11, 11, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (3, 3, 3, NULL, NULL, 'RUE', 'dzadza', NULL, '91 rue d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (15, 16, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (14, 15, 14, '1', NULL, 'AV', 'de Paris', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (16, 17, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (17, 18, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (18, 19, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (19, 20, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (20, 21, 20, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (21, 22, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '14', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (22, 23, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (23, 24, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (24, 26, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (25, 27, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (26, 29, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (27, 30, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (12, 13, 12, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (13, 14, 13, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (31, 35, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (30, 34, 30, '1', NULL, 'AV', 'de Paris', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (32, 36, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (33, 37, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (34, 38, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (35, 39, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (36, 40, 36, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (37, 41, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '77', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (38, 42, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (39, 43, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (40, 45, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (41, 46, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (42, 48, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (43, 49, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (28, 32, 28, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (29, 33, 29, '3', NULL, 'AV', 'de versailles', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (44, 51, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (45, 52, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (46, 53, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO adresse VALUES (47, 54, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

---------Table etablissement ------------
INSERT INTO etablissement VALUES (1, 5, NULL, NULL, NULL, '1', '3', NULL, '2016-11-16', 'P', NULL, NULL, NULL, NULL, NULL, NULL, 'dzadza', NULL, NULL, 'dzadza', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'creation', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, false, NULL);
INSERT INTO etablissement VALUES (2, 14, NULL, NULL, NULL, '1', '3', NULL, '2015-04-01', 'P', NULL, NULL, NULL, NULL, 'nonConcerne', NULL, 'Reparation et vente de vehicules', NULL, NULL, 'Reparation et vente de vehicules', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'BEP Mecanique', '99', 'Reparation de vehicule', NULL, NULL, NULL, 'EIRL Dupont Rene', 'Garage DR Chartres', 'achat', NULL, NULL, NULL, NULL, NULL, false, false, 'JAL Chartrain', '2015-03-02', 0, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, false, false, false, false, NULL);
INSERT INTO etablissement VALUES (3, 30, NULL, NULL, NULL, '1', '3', NULL, '2015-04-01', 'P', NULL, NULL, NULL, NULL, 'nonConcerne', NULL, 'Reparation et vente de vehicules', NULL, NULL, 'Reparation et vente de vehicules', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'BEP Mecanique', '99', 'Reparation de vehicule', NULL, NULL, NULL, 'EIRL Dupont Rene', 'Garage DR Chartres', 'achat', NULL, NULL, NULL, NULL, NULL, false, false, 'JAL Chartrain', '2015-03-02', 0, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, false, false, false, false, NULL);
INSERT INTO etablissement VALUES (4, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, false, false, false, NULL);

---------Table personnephysique ------------
INSERT INTO personnephysique VALUES (1, 9, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (2, 11, 10, NULL, NULL, 'Dzadza', NULL, NULL, '2016-11-16', '2', '050', NULL, 'dzadza', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (3, NULL, NULL, NULL, NULL, 'Durant', NULL, NULL, NULL, NULL, NULL, NULL, 'yannickk', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (4, 19, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (5, 21, 20, NULL, NULL, 'Dupont', NULL, NULL, '1970-01-01', '1', '050', NULL, 'Rene', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (6, 23, 22, NULL, NULL, 'Durand', 'Dupont', NULL, '1980-01-01', NULL, '050', NULL, 'Annie', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (7, 25, 24, NULL, NULL, 'Dupon', NULL, NULL, NULL, NULL, '050', NULL, 'Yoan', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (8, 27, 26, NULL, NULL, 'Durand', NULL, NULL, '2001-01-01', '2', '050', NULL, 'Lea', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (9, NULL, NULL, NULL, NULL, 'Durant', NULL, NULL, NULL, NULL, NULL, NULL, 'yannickk', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (10, 35, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (11, 37, 36, NULL, NULL, 'Dupont', NULL, NULL, '1970-01-01', '1', '050', NULL, 'Rene', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (12, 39, 38, NULL, NULL, 'Durand', 'Dupont', NULL, '1980-01-01', NULL, '050', NULL, 'Annie', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (13, 41, 40, NULL, NULL, 'Dupon', NULL, NULL, NULL, NULL, '050', NULL, 'Yoan', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnephysique VALUES (14, 43, 42, NULL, NULL, 'Durand', NULL, NULL, '2001-01-01', '2', '050', NULL, 'Lea', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

---------Table personnemorale ------------
INSERT INTO personnemorale VALUES (1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnemorale VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnemorale VALUES (3, 17, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnemorale VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO personnemorale VALUES (5, 33, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL);

---------Table entrepriseliee ------------
INSERT INTO entrepriseliee VALUES (1, 5, 16, NULL, 2, 3, NULL, NULL, 1, NULL, '111111159', NULL, NULL, 'PrecedentExploitant', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entrepriseliee VALUES (2, 7, 32, NULL, 4, 9, NULL, NULL, 1, NULL, '111111159', NULL, NULL, 'PrecedentExploitant', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

---------Table personnemorale ------------
INSERT INTO personne VALUES (1, 1, 2);
INSERT INTO personne VALUES (2, 3, 5);
INSERT INTO personne VALUES (3, NULL, 6);
INSERT INTO personne VALUES (4, 5, 11);
INSERT INTO personne VALUES (5, NULL, 12);

---------Table conjoint ------------
INSERT INTO conjoint VALUES (1, 3);
INSERT INTO conjoint VALUES (2, 5);

---------Table entreprise ------------
INSERT INTO entreprise VALUES (1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (3, NULL, NULL, 6, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, 2, NULL, NULL, '[]', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (5, NULL, NULL, 15, NULL, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, true, 'Chartes', false, NULL, false, NULL, true, 2, NULL, NULL, '[]', NULL, NULL, NULL, NULL, true, true, NULL);
INSERT INTO entreprise VALUES (6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (7, NULL, NULL, 31, NULL, 2, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, true, 'Chartes', false, NULL, false, NULL, true, 2, NULL, NULL, '[]', NULL, NULL, NULL, NULL, true, true, NULL);
INSERT INTO entreprise VALUES (8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO entreprise VALUES (9, NULL, NULL, 47, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, false, NULL, NULL, true, 'Chartes', false, NULL, false, NULL, true, 2, NULL, NULL, '[]', NULL, NULL, NULL, NULL, true, true, NULL);

---------Table observation ------------
INSERT INTO observation VALUES (1, NULL);
INSERT INTO observation VALUES (2, NULL);
INSERT INTO observation VALUES (3, 'Observation- Demande d''ACCRE');
INSERT INTO observation VALUES (4, 'Observation- Demande d''ACCRE');
INSERT INTO observation VALUES (5, NULL);

---------Table evenement ------------
INSERT INTO evenement VALUES ('COG', 'Commerce de gros', 'formalite', '111');
INSERT INTO evenement VALUES ('DSA', 'Déclaration sanitaire', 'formalite', '111');
INSERT INTO evenement VALUES ('GIT', 'Gîte rural', 'formalite', '111');
INSERT INTO evenement VALUES ('HOT', 'Hôtel', 'formalite', '111');
INSERT INTO evenement VALUES ('AGM', 'Agence de mannequins', 'formalite', '111');
INSERT INTO evenement VALUES ('CHO', 'Chambres d''hôtes', 'formalite', '111');
INSERT INTO evenement VALUES ('DBI', 'Débit de boissons', 'formalite', '111');
INSERT INTO evenement VALUES ('DEC', 'Demande d''autorisation d''exploitation commerciale', 'formalite', '111');
INSERT INTO evenement VALUES ('DRS', 'Dérogation sanitaire', 'formalite', '111');
INSERT INTO evenement VALUES ('FIT', 'Fiche technique', 'formalite', '111');
INSERT INTO evenement VALUES ('01P', 'Création d''une entreprise individuelle', 'registre', '111');
INSERT INTO evenement VALUES ('02M', 'Constitution d''une société sans activité', 'registre', '111');
INSERT INTO evenement VALUES ('03M', 'Constitution d''une société sans activité au siège avec début d''activité hors siège', 'registre', '111');
INSERT INTO evenement VALUES ('05P', 'Création d''une entreprise individuelle, personne ayant déjà exercé une activité non salariée', 'registre', '111');
INSERT INTO evenement VALUES ('ADA', 'Artisan d''art', 'dialogues', '111');
INSERT INTO evenement VALUES ('ADB', 'Administrateur de biens', 'dialogues', '111');
INSERT INTO evenement VALUES ('AIM', 'Agent immobilier', 'dialogues', '111');
INSERT INTO evenement VALUES ('AVA', 'Achat/Vente d''animaux', 'dialogues', '111');
INSERT INTO evenement VALUES ('BRO', 'Antiquaire-Brocanteur', 'dialogues', '111');
INSERT INTO evenement VALUES ('CDT', 'Commissionnaire de transport', 'dialogues', '111');
INSERT INTO evenement VALUES ('CFF', 'Courtier de fret fluvial', 'dialogues', '111');
INSERT INTO evenement VALUES ('CTC', 'Contrôleur technique de la construction', 'dialogues', '111');
INSERT INTO evenement VALUES ('CVS', 'Courtier en vins et spiritueux', 'dialogues', '111');
INSERT INTO evenement VALUES ('DEB', 'Débit de boissons', 'dialogues', '111');
INSERT INTO evenement VALUES ('DEV', 'Depôt-Vente', 'dialogues', '111');
INSERT INTO evenement VALUES ('ECO', 'Expert comptable', 'dialogues', '111');
INSERT INTO evenement VALUES ('EFO', 'Expert forestier', 'dialogues', '111');
INSERT INTO evenement VALUES ('GAD', 'Garde d''animaux', 'dialogues', '111');
INSERT INTO evenement VALUES ('GAR', 'Garagiste', 'dialogues', '111');
INSERT INTO evenement VALUES ('GCO', 'Guide conférencier', 'dialogues', '111');
INSERT INTO evenement VALUES ('GEO', 'Géomètre expert', 'dialogues', '111');
INSERT INTO evenement VALUES ('LAV', 'Laverie libre-service', 'dialogues', '111');
INSERT INTO evenement VALUES ('RTA', 'Restauration traditionnelle', 'dialogues', '111');
INSERT INTO evenement VALUES ('TEI', 'Teinturerie', 'dialogues', '111');
INSERT INTO evenement VALUES ('TOI', 'Toilettage d''animaux', 'dialogues', '111');
INSERT INTO evenement VALUES ('VTE', 'Vente à emporter', 'dialogues', '111');
INSERT INTO evenement VALUES ('ARA', 'Galerie d''art', 'formalite', '111');
INSERT INTO evenement VALUES ('CCA', 'Ambulant', 'formalite', '111');
INSERT INTO evenement VALUES ('DED', 'Déclaration d''existence du diffuseur', 'formalite', '111');
INSERT INTO evenement VALUES ('MDA', 'Maison des artistes', 'formalite', '111');
INSERT INTO evenement VALUES ('01M', 'Création d''une entreprise personne morale', 'registre', '111');
INSERT INTO evenement VALUES ('ARC', 'Architecte', 'formalite', '111');
INSERT INTO evenement VALUES ('VET', 'Vétérinaire', 'formalite', '111');
INSERT INTO evenement VALUES ('ECD', 'Ecole de danse', 'formalite', '111');
INSERT INTO evenement VALUES ('PDD_LE', 'Professeur de danse (Libre Etablissement)', 'formalite', '010');
INSERT INTO evenement VALUES ('PDD_LPS', 'Professeur de danse (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('REC_LE', 'Recouvrement de créances (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('REC_LPS', 'Recouvrement de créances (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('SDD', 'Société de domiciliation', 'formalite', '111');
INSERT INTO evenement VALUES ('SEF_LE', 'Services funéraires (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('SEF_LPS', 'Services funéraires (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('VAEE', 'Vente aux enchères (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('VAES', 'Vente aux enchères (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('AAB', 'Désossage', 'formalite', '111');
INSERT INTO evenement VALUES ('EDS_LE', 'Educateur sportif', 'formalite', '111');
INSERT INTO evenement VALUES ('EDS_LPS', 'Educateur sportif', 'formalite', '111');
INSERT INTO evenement VALUES ('OFS_LPS', 'Organisateur de foires et salons (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('OFS_LE', 'Organisateur de foires et salons (Libre Etablissement)', 'formalite', '100');
INSERT INTO evenement VALUES ('PIS', 'Exploitant de piscine', 'formalite', '111');
INSERT INTO evenement VALUES ('AUE', 'Demande d’agrément d’un établissement d’enseignement de la conduite des véhicules à moteur et de la sécurité routière', 'formalite', '111');
INSERT INTO evenement VALUES ('CDF_LE', 'Centre de formation (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('CDF_LPS', 'Centre de formation (Libre Prestation de Services)', 'formalite', '111');
INSERT INTO evenement VALUES ('CDH', 'Collecte déchet huile', 'formalite', '111');
INSERT INTO evenement VALUES ('CDP', 'Collecte déchet pneu', 'formalite', '111');
INSERT INTO evenement VALUES ('DAE', 'Demande d’autorisation d’enseigner à titre onéreux la conduite des véhicules à moteur et la sécurité routière', 'formalite', '111');
INSERT INTO evenement VALUES ('DIS', 'Discothèque', 'formalite', '111');
INSERT INTO evenement VALUES ('GRA', 'Attestation Groupement national interprofessionnel des semences et plants', 'formalite', '111');
INSERT INTO evenement VALUES ('PHY', 'Demande d’agrément concernant les produits phytopharmaceutiques', 'formalite', '111');
INSERT INTO evenement VALUES ('SDS_LE', 'Salle de sport (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('TDH', 'Traitement huiles', 'formalite', '111');
INSERT INTO evenement VALUES ('TDP', 'Traitement pneus', 'formalite', '111');
INSERT INTO evenement VALUES ('EFA', 'Expert foncier agricole', 'dialogues', '111');
INSERT INTO evenement VALUES ('EAU_LE', 'Expert automobile (Libre Etablissement)', 'formalite', '111');
INSERT INTO evenement VALUES ('EAU_LPS', 'Expert automobile (Libre Prestation de Service)', 'formalite', '111');
INSERT INTO evenement VALUES ('PP', 'UCfe0000059', 'dialogues', '111');
INSERT INTO evenement VALUES ('SE.PAY_LE', 'Demande d’agrément concernant les produits phytopharmaceutiques', 'formalite', '111');
INSERT INTO evenement VALUES ('SE.PAY_LPS', 'Générique', 'formalite', '111');

---------Table formalite ------------
--INSERT INTO formalite VALUES (NULL, 13, 'C14010002012', 12, NULL, '0', 1, 3, NULL, '01P', NULL, 5, 'M', false, true, '01234569871', NULL, NULL, 'khadhir@yopmail.com', 'dupon Rene', NULL, '2017-01-12', 'Chartres', 'Dupon René', NULL, 0, 'M2801','28085', false, false, true, '111', true, true);


---------Table notification_purge ------------
INSERT INTO notification_purge VALUES (1, 'DOSSIER_C20170307153', '0', now(), false);
